// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Interface_Interact.generated.h"

// This class does not need to be modified.
UINTERFACE(BlueprintType)
class UInterface_Interact : public UInterface
{
	GENERATED_BODY()
};

/**
 * Interfaec for allowing players to "interact" with multiple different types of actors
 */
class NETWORK_TEST_API IInterface_Interact
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION()
		virtual void Interact(); // TODO: Make this return something so we know what, if anything, we interacted with.
};
