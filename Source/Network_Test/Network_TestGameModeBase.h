// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Network_TestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class NETWORK_TEST_API ANetwork_TestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
