// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Interface_Interact.h"
#include "UnrealNetwork.h"
#include "Core.h"
#include "Engine.h"
#include "GameFramework/Actor.h"
#include "Interactable.generated.h"

UCLASS()
class NETWORK_TEST_API AInteractable : public AActor, public IInterface_Interact
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractable();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// All interactables are either active or inactive
	UPROPERTY(replicated, EditAnywhere, BlueprintReadWrite)
		bool active;

	// Implement the interface and make it available to blueprints
	UFUNCTION(BlueprintCallable)
		void Interact() override;
	// For ease of use, make this a blueprint event
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void OnInteract();
};
